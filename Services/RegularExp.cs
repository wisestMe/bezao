﻿using System.Text.RegularExpressions;
using System;

namespace EventsAndEventHandlers.Services
{
    /// <summary>
    /// Purpose: to locate substrings within a large string expression
    /// 
    /// Features: 
    /// 1) set of escaping codes for identifying specific type of charaters
    /// 2) A system for grouping parts of substrings and intermediate results during a search operation
    /// </summary>
    public class RegularExp
    {
        const string SearchString = @"Professional C# 6 and .NET Core 1.0 provides complete coverage " +
                                     "of the latest updates, feature, and capabilities, giving you " +
                                     "everything you need for C#. Get expert instruction on the latest " +
                                     "changes to Visual Studio 2015, Windows Runtime, ADO.NET, ASP.NET, " +
                                     "Windows Store Apps, Windows Workflow Foundation, and more, with " +
                                     "clear explanations, no-nonsense pacing, and valuable expert insight. " +
                                     "This incredibly useful guide serves as both tutorial and desk " +
                                     "reference, providing a professional-level review of C# architecture " +
                                     "and its application in a number of areas. You'll gain a solid " +
                                     "background in managed code and .NET constructs within the context of " +
                                     "the 2015 release, so you can get acclimated quickly and get back to work.";

        public static void MatchSample()
        {
            string pattern = "ion";


            MatchCollection matches = Regex.Matches(SearchString, pattern, RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);


            WriteMatches(SearchString, matches);
        }

        private static void WriteMatches(string searchString, MatchCollection matches)
        {
            Console.WriteLine($"No of matches {matches.Count}");

            foreach (Match matchedItem in matches)
            {
                int index = matchedItem.Index;
                string result = matchedItem.ToString();
                int charsBefore = (index < 5) ? index : 5;
                int fromEnd = searchString.Length - (index + result.Length);
                int charsAfter = (fromEnd < 5) ? fromEnd : 5;
                int charsToDisplay = charsBefore + charsAfter + result.Length;

                Console.WriteLine($"Index: {index}, \tString: {result}, \t {searchString.Substring(index - charsBefore, charsToDisplay)}");


            }

        }

        public static void Example()
        {
            //End mataching
            const string pattern = @"ure\b";

            MatchCollection matches = Regex.Matches(SearchString, pattern,
               RegexOptions.IgnoreCase);
            WriteMatches(SearchString, matches);


            // \S escaping and * quantifier
            const string pattern1 = @"\ba\S*ure\b";


        }

        public static void BeginAndEndMatching()
        {
            string pattern = @"(\ba)(\S*)(ure\b)";

            MatchCollection matches = Regex.Matches(SearchString, pattern,
            RegexOptions.IgnoreCase);
            WriteMatches(SearchString, matches);

           
        }

        public static void GroupMatching(string url)
        {
            string pattern = @"\b(https?)(://)([.\w]+)([\s:]([\d]{2,5})?)\b";

            MatchCollection matches = Regex.Matches(url, pattern,
            RegexOptions.IgnoreCase);
            WriteMatches(url, matches);
        }

    }
}
