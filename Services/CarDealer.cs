﻿using EventsAndEventHandlers.Models;
using System;


namespace EventsAndEventHandlers.Services
{
    /// <summary>
    /// Events uses methods with two parameters; 
    /// the first parameter is an object and contains the sender of the event, 
    /// and the second parameter provides information about the event
    /// </summary>
    public class CarDealer
    {
        public event EventHandler<CarInfoArgs> NewCarAlert;

        public void NewCar(Car car)
        {
            Console.WriteLine($"Car Dealer, new car alert ...");
            Console.WriteLine($"Car Name: {car.Name} \n Car Model: {car.ModelNumber} \n Manufactured Date: {car.Date}");

            NewCarAlert?.Invoke(this, new CarInfoArgs(car));
        }

        public Car InitiateNewCar()
        {
            try
            {
                DateTime dateOfMaking = DateTime.Now;
                Console.WriteLine("You are about to annouces a new car to subscribed customers, Enter car Name, Model Number and Date of making in this order.");

                Car newCar = new Car();

                Console.WriteLine("Enter Car Name");
                newCar.Name = Console.ReadLine();

                Console.WriteLine("Enter Car Model Number, ensure to enter just number");
                newCar.ModelNumber = int.Parse(Console.ReadLine());

                Console.WriteLine("Enter Date of Making, ensure to enter valid date dd/mm/yyyy");
                DateTime.TryParse(Console.ReadLine(), out dateOfMaking);
                newCar.Date = dateOfMaking;

                return newCar;
            }
            catch (Exception)
            {

                throw;
            }

        }
        protected void RaiseNewCarInfo(Car car)
        {
            NewCarAlert?.Invoke(this, new CarInfoArgs(car));
        }
    }
}
