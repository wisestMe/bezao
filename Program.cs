﻿using EventsAndEventHandlers.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsAndEventHandlers
{
    class Program
    {
        static void Main(string[] args)
        {
            //Calling the closure implementation
            //Closure.DisplayResult();

            // Event Example
            //CarDealer dealer = new CarDealer();
            //var car = dealer.InitiateNewCar();

            ////Adding new customer
            //var max = new CustomerService("Max");
            //dealer.NewCarAlert += max.NewCarIsHere;
            //dealer.NewCar(car);

            //car.Name = "Mercedes";

            //var mikey = new CustomerService("Mikey");
            //dealer.NewCarAlert += mikey.NewCarIsHere;
            //dealer.NewCar(car);

            //car.Name = "Ferrari";

            //dealer.NewCarAlert -= max.NewCarIsHere;
            //dealer.NewCar(car);


            //BuildingStrings.example();

            //RegularExp.MatchSample();

            //RegularExp.Example();

            ////Begin and End Match
            //RegularExp.BeginAndEndMatching();

            //string myName = "Nzeh Davison Chiadikaobi";

            // Console.WriteLine(myName.Substring(5, 7));

            string url = "http://www.wrox.com:30";

            RegularExp.GroupMatching(url);

            Console.ReadLine();
        }
    }
}
