﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsAndEventHandlers.Models
{
    public class Car
    {
        public int ModelNumber { get; set; }
        public string Name { get; set; }
        public DateTime? Date { get; set; }
    }
}
